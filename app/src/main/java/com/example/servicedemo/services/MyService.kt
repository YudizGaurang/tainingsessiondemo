package com.example.servicedemo.services

import android.app.IntentService
import android.content.Intent
import android.os.Binder
import android.os.IBinder
import android.util.Log
import com.example.servicedemo.R
import java.io.FileDescriptor
import java.io.PrintWriter
import java.util.*

class MyService : IntentService("MyService") {
    private var mRandomNumber = 0
    private var mIsRandomGeneratorOn = false
    private val MIN = 0
    private val MAX = 100
    private val mBinder: IBinder = MyServiceBinder()
    val GET_COUNT = 0

    override fun onBind(intent: Intent?): IBinder? {
        Log.i(getString(R.string.service_demo_tag), "In OnBind")
        return mBinder
//        if (intent!!.getPackage() === "serviceclientapp.youtube.com.messengerserviceclientapp") {
//            randomNumberMessenger.getBinder()
//        } else {

//        }
    }

    override fun onStart(intent: Intent?, startId: Int) {
        super.onStart(intent, startId)
        Log.i(getString(R.string.service_demo_tag), "Service Started")
    }

 /*   override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {
        Log.i(
            getString(R.string.service_demo_tag),
            "In onStartCommend, thread id: " + Thread.currentThread().id
        )

        return START_STICKY
    }*/

    override fun onRebind(intent: Intent?) {
        super.onRebind(intent)
        Log.i(getString(R.string.service_demo_tag), "In OnReBind")
    }

    inner class MyServiceBinder : Binder() {
        fun getMyService(): MyService {
            return this@MyService
        }
    }

    private fun startRandomNumberGenerator() {
        while (mIsRandomGeneratorOn) {
            try {
                Thread.sleep(1000)
                if (mIsRandomGeneratorOn) {
                    mRandomNumber = Random().nextInt(MAX) + MIN
                    Log.i(
                        getString(R.string.service_demo_tag),
                        "Thread id: " + Thread.currentThread().id + ", Random Number: " + mRandomNumber
                    )
                }
            } catch (e: InterruptedException) {
                Log.i(getString(R.string.service_demo_tag), "Thread Interrupted")
            }
        }
    }

    private fun stopRandomNumberGenerator() {
        mIsRandomGeneratorOn = false
    }

    override fun onUnbind(intent: Intent?): Boolean {
        Log.i(getString(R.string.service_demo_tag), "In onUnbind")
        return super.onUnbind(intent)
    }

    fun getRandomNumber(): Int {
        return mRandomNumber
    }


    override fun onDestroy() {
        super.onDestroy()
        stopRandomNumberGenerator()
        Log.i(getString(R.string.service_demo_tag), "Service Destroyed")
    }

    override fun dump(fd: FileDescriptor, fout: PrintWriter, args: Array<out String>?) {}

    override fun onHandleIntent(intent: Intent?) {
        Log.i(
            getString(R.string.service_demo_tag),
            "In onHandleIntent, thread id: " + Thread.currentThread().id
        )
        mIsRandomGeneratorOn = true
        startRandomNumberGenerator()
    }

}