package com.example.servicedemo

import android.content.ComponentName
import android.content.Intent
import android.content.ServiceConnection
import android.os.Bundle
import android.os.IBinder
import android.util.Log
import android.view.View
import android.widget.Button
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import com.example.servicedemo.services.MyService

class MainActivity : AppCompatActivity(), View.OnClickListener {

    private val TAG = MainActivity::class.java.simpleName
    private var textViewthreadCount: TextView? = null
    var count = 0
    private var myService: MyService? = null
    private var isServiceBound = false
    private var serviceConnection: ServiceConnection? = null
    private var serviceIntent: Intent? = null
    private var mStopLoop = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        Log.i(
            getString(R.string.service_demo_tag),
            "MainActivity thread id: " + Thread.currentThread().id
        )

        val buttonStart = findViewById<Button>(R.id.buttonThreadStarter)
        val buttonStop = findViewById<Button>(R.id.buttonStopthread)
        val buttonBind = findViewById<Button>(R.id.buttonBindService)
        val buttonUnBind = findViewById<Button>(R.id.buttonUnBindService)
        val buttonGetRandomNumber = findViewById<Button>(R.id.buttonGetRandomNumber)

        textViewthreadCount = findViewById(R.id.textViewthreadCount)

        buttonStart.setOnClickListener(this)
        buttonStop.setOnClickListener(this)
        buttonBind.setOnClickListener(this)
        buttonUnBind.setOnClickListener(this)
        buttonGetRandomNumber.setOnClickListener(this)

        serviceIntent = Intent(
            applicationContext,
            MyService::class.java
        )

    }


    override fun onClick(view: View?) {
        when (view?.getId()) {
            R.id.buttonThreadStarter -> {
                mStopLoop = true
                startService(serviceIntent)
            }
            R.id.buttonStopthread -> stopService(serviceIntent)
            R.id.buttonBindService -> bindService()
            R.id.buttonUnBindService -> unbindService()
            R.id.buttonGetRandomNumber -> setRandomNumber()
        }
    }


    private fun bindService() {
        if (serviceConnection == null) {
            serviceConnection = object : ServiceConnection {
                override fun onServiceConnected(componentName: ComponentName, iBinder: IBinder) {
                    val myServiceBinder: MyService.MyServiceBinder = (iBinder as MyService.MyServiceBinder)
                    myService = myServiceBinder.getMyService()
                    isServiceBound = true
                }

                override fun onServiceDisconnected(componentName: ComponentName) {
                    isServiceBound = false
                }
            }
        }
        bindService(serviceIntent, serviceConnection!!, BIND_AUTO_CREATE)
    }

    private fun unbindService() {
        if (isServiceBound) {
            unbindService(serviceConnection!!)
            isServiceBound = false
        }
    }

    private fun setRandomNumber() {
        if (isServiceBound) {
            textViewthreadCount!!.text = "Random number: " + myService!!.getRandomNumber()
        } else {
            textViewthreadCount!!.text = "Service not bound"
        }
    }
}